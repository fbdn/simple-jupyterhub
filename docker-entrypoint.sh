#!/bin/bash

# List of users is passed by docker
for username in $USERS; do
  # Create user and link its home directory
  useradd -m $username -d /home/$username -s /bin/bash
  # Chown home directory with username
  chown -R $username:$username /home/$username
done

for username in $SUDOERS; do
  echo "Adding $username to sudoers group..."
  usermod -aG sudo $username
done

# Add admin users from ENV to JupyterHub config
ADMIN_CONFIG=c.Authenticator.admin_users\={`for i in $ADMINS; do printf \'$i\', ; done`}
echo $ADMIN_CONFIG >> /opt/jupyterhub/etc/jupyterhub/jupyterhub_config.py 

# Run JupyterHub
/opt/jupyterhub/bin/jupyterhub -f /opt/jupyterhub/etc/jupyterhub/jupyterhub_config.py
