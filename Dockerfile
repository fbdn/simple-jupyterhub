from ubuntu:18.04
ENV DEBIAN_FRONTEND noninteractive
ARG PYTHON_VERSION=3.8

# Install dependencies for python and various tools
RUN apt update && apt upgrade -y && \
    apt install -y python3 python3-pip python3-venv \
        nodejs npm sudo nmap curl vim wget

# Install http proxy for jupyter and 'n' to manage node installations
RUN npm install -g configurable-http-proxy n 

# Install latest node release with 'n'
RUN n latest


# Create python virtual env and basic config for jupyterhub server
RUN python3 -m venv /opt/jupyterhub/ && \
    /opt/jupyterhub/bin/python3 -m pip install \
        wheel notebook jupyterhub jupyterlab jupytext \
        jupyterhub-ldapauthenticator ipywidgets && \
    mkdir -p /opt/jupyterhub/etc/jupyterhub/ && \
    cd /opt/jupyterhub/etc/jupyterhub/ && \
    /opt/jupyterhub/bin/jupyterhub --generate-config && \
    echo c.Spawner.cmd = "'/opt/jupyterhub/bin/jupyterhub-singleuser'" \
    >> jupyterhub_config.py


# Install + configure conda for user environments
RUN echo "deb [arch=amd64] https://repo.anaconda.com/pkgs/misc/debrepo/conda stable main" \
    > /etc/apt/sources.list.d/conda.list && \
    curl https://repo.anaconda.com/pkgs/misc/gpgkeys/anaconda.asc | gpg --dearmor > conda.gpg && \
    install -o root -g root -m 644 conda.gpg /etc/apt/trusted.gpg.d/ && \
    apt update && apt install -y conda && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh 

# Update conda
RUN /opt/conda/bin/conda update -n base -c defaults conda


RUN mkdir /opt/conda/envs/ && \
    /opt/conda/bin/conda create --prefix /opt/conda/envs/python python ipykernel && \
    /opt/conda/envs/python/bin/python -m ipykernel install --prefix /usr/local/ --name 'python'


ADD docker-entrypoint.sh /docker-entrypoint.sh
CMD ["bash", "/docker-entrypoint.sh"]
