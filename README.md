# Simple JupyterHub in Docker

_Dockerized JupyterHub with instructions to install various kernels and extensions_

__NOTE__: This image is not intended for use on a publicly exposed server.


## Usage
#### docker-compose

~~~bash
git clone git clone git@gitlab.com:fbdn/simple-jupyterhub.git
docker-compose up -d && docker-compose logs -f
~~~

#### Docker only

~~~bash
docker run --name jupyterhub
    --env-file .env \
    -p 8000:8000 \
    -v `pwd`/home:/home \
    registry.gitlab.com/fbdn/simple-jupyterhub
~~~

## Custom Kernels
Users can assemble their own Jupyter Kernel by creating a conda environment with 
their preferred packages and register it to the Hub. 
The env resides in the user home directory to persists docker recreates.  
Therefore, on each reboot the home directories are `chowned` to the user. 

To initiate a conda configuration for your user, 
open a Terminal console from the 
[JupyterHub tree](http://0.0.0.0:8000/) (New > Terminal) and run:

~~~bash
echo PATH=$PATH:/opt/jupyterhub/bin/ >> ~/.bashrc
conda init && bash
~~~

This will set up conda for you and reload the shell, 
so you can use conda to create new environments.  
Also this adds the `jupyter` cli-programs to your path.
Then run the corresponding commands for each language, see below.



### Python

Create and activate a new conda environment with python and the ipykernel plugin:

~~~bash
conda create -y --name MyPythonEnv python=3.8 ipykernel 
conda activate MyPythonEnv
~~~

The new Environment will live in `~/.conda/envs/MyPythonEnv`.   
Now to install custom packages and register the kernel for jupyter:

~~~bash
conda install -y psycopg2 requests pandas numpy matplotlib
python -m ipykernel install --user --name=MyPythonKernel
~~~

### R 
The above applys for R kernels too, with minimal deviation.
Open up a terminal and run:

Create and activate the conda R kernel

~~~bash
conda create -y --name RwithFortunes r r-irkernel -y
conda activate RwithFortunes
~~~

Install desired packages and register the kernel to jupyter

~~~bash
R -e "install.packages(c('fortunes', 'cowsay'), repos='http://cran.rstudio.com')"
R -e "IRkernel::installspec(displayname='RwithFortunes', user=TRUE)"
~~~

Now fire up a notebook with the new kernel and use the libs as normal:

~~~r
library('fortunes')
library("cowsay")
say(paste(fortune(), collapse="\n"))
~~~


### Julia 
The Julia ecosystem can be used in JupyterHub with [IJulia](https://github.com/JuliaLang/IJulia.jl).  
Create and activate an empty conda environment:

~~~bash
conda create -y --name=MyJuliaEnv && conda activate MyJuliaEnv
~~~

Find your preferred Julia Version here: https://julialang.org/downloads/   
Download and unpack the Julia build to your environment folder:

~~~bash
wget https://julialang-s3.julialang.org/bin/linux/x64/1.5/julia-1.5.0-linux-x86_64.tar.gz
tar -xzvf julia-1.5.0-linux-x86_64.tar.gz -C ~/.conda/envs/MyJuliaEnv/
rm julia-1.5.0-linux-x86_64.tar.gz
~~~

Run Julia from cli to install _IJulia_ and register the kernel:

~~~bash
cd ~/.conda/envs/MyJuliaEnv/julia-1.5.0/bin
./julia -e 'using Pkg; Pkg.add("IJulia"); Pkg.add("Plots")'
./julia -e 'using IJulia; IJulia.installkernel("MyJuliaEnv")'
~~~


## Help
To manage your kernels use `jupyter kernelspec`.   
For example to see what kernels you have registered 
and where the interpreters live, run

~~~bash
jupyter kernelspec list
~~~

If you are in doubt what interpreter you currently use, 
activate the Env and run which:

~~~bash
conda activate MyPythonEnv
which python
~~~


### Add notebook extensions
One can simply add various [Notebook extensions
](https://github.com/ipython-contrib/jupyter_contrib_nbextensions) 
by installing them to the user environment:

~~~bash
conda activate MyCondaEnv
# Install extension system
conda install -c conda-forge jupyter_contrib_nbextensions
# Copy extension files to the users jupyter folder
jupyter nbextensions_configurator enable --user
# To add e.g. codefolding:
jupyter nbextension enable codefolding/main
~~~

To install external extensions via git, see e.g. [jupyter-vim-binding](https://github.com/lambdalisue/jupyter-vim-binding).

#### List extensions

By default extension files are places inside the users jupyter folder:

~~~bash
ls $(jupyter --data-dir)/nbextensions/
~~~

### User Administration
To persist user data when recreating the docker container,
the entire folder `/home` is mounted to the host system.

On each boot, the `docker-entrypoint.sh` creates a unix user 
and the home directory for each entry found in the variable `$USERS`.
If the directory already exists, it is `chowned` for the corresponding user 
identified by the username.

_NOTE:_ The uid's are incremented from 1000, therefore the uid of a 
process inside the container may be resolved to a username on the host system.


#### Set user password
For security reasons there is no mechanism to persists user passwords,
these need to be set on each restart:


This will simply prompt for the password:
~~~bash
docker exec -it jupyterhub_jupyter_1 passwd pyter
~~~


